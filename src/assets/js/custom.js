export var footerFix = function () {
  // var docHeight = $(window).height();
  // var footerHeight = $('footer').height();
  // var footerTop = $('footer').position().top + footerHeight;
  //
  // if (footerTop < docHeight) {
  //   $('footer').css('margin-top', 10+ (docHeight - footerTop) + 'px');
  // }
};
export var inputFocus = function () {
  $("#addLineBtn").click(function() {
    $('#addLineModal').on('shown.bs.modal', function() {
      $('#add-line').focus();
    })
  });
};


export var enterNumber = function(){
  $(".enterNumber").keypress(function (eve) {
    //if the letter is not digit then display error and don't type anything
    if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0) ) {
      eve.preventDefault();
    }
  });
}


export var daterange = function(){
  var minDate = new Date();
  $('.daterange').daterangepicker({
    // minDate : minDate
  });
  var maxDate = new Date();
  $('.daterangemax').daterangepicker({
    // maxDate : maxDate
  });
}


 export var tabactive = function (){
  var selector = '.nav-tabs li';

  $(selector).on('click', function(){
      $(selector).removeClass('active');
      $(this).addClass('active');
  });
 }
 export var closesidebar = function (){
  $('.sidebar li').click(function(){
    $(".sidebar").css("display", "none");
    $(".nav-open .main-panel").css("transform", "unset");
    $(".main-panel").css("width", "100%");
});
 }



export var initMap = function (a,b) {
  var uluru = { lat: a, lng: b };
  var map = new google.maps.Map(
    document.getElementById('map'), { zoom: 14, center: uluru });
  var marker = new google.maps.Marker({ position: uluru, map: map });
}

  
