import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {LocalStorageService} from 'angular-web-storage';
import {PaginationBody} from '../../../requests/pagination-body';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AppComponent} from '../../../app.component';
import { AmazingTimePickerService } from 'amazing-time-picker';

declare var $:any;
import * as _ from 'lodash';
import { UrlService } from 'src/app/services/url.service';
import { Router } from '@angular/router';
import * as js from 'src/assets/js/custom';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {

  paginationBody = new PaginationBody();
  currentPage = 0;
  serialNumber: number;
  totalItems: number;
  flags = {
    isAdded: false
  };
  getList:any;

  src3:any;
  day:string;
  timeFrom:string;
  timeTo:string;
  openingHoursData=[];
  dishImage:File;
  restaurantId:string;
  imageUrl:string;
  user:string;
  completedOrderDetails:any;
  restaurantSection:number;
  orderData=[];
  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private atp: AmazingTimePickerService,
    private url:UrlService,
    private router:Router
  ) { }

  ngOnInit() {
    js.tabactive();
    this.restaurantSection = this.localStorage.get('restaurantSection')
   
    this.imageUrl = this.url.imageUrl;
    this.totalItems = 0;
    this.serialNumber = 0;
    this.paginationBody.limit = 10;
    this.paginationBody.page = 0;
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.app.show();
    this.getData();
  }
  getData() {
      const data  = this.localStorage.get('orderDetail');
      console.log(data)
      this.getList = data
      this.app.hide();
  }


  onChangeLimit() {
    this.app.show();
    this.getData();
  }

  pageChanged(e) {
    this.paginationBody.page = e.pageIndex;
    this.paginationBody.limit = e.pageSize;
    this.app.show();
    this.serialNumber = this.paginationBody.limit * this.paginationBody.page;
    this.getData();
  }

  


  error = (message: string) => {
    this.toaster.errorToastr(message);
  }

}


