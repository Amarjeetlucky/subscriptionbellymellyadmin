import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LocalStorageService } from 'angular-web-storage';
import { PaginationBody } from '../../requests/pagination-body';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UserList } from '../../model/userList';
import { AppComponent } from '../../app.component';
import { User } from '../../model/user';
import { AmazingTimePickerService } from 'amazing-time-picker';
declare var $: any;
declare var swal: any;
import * as _ from 'lodash';
import { addBody } from '../../requests/dish/add';
import { UrlService } from 'src/app/services/url.service';
import { Router } from '@angular/router';
import { ObservableService } from 'src/app/services/observable.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  user = new User();
  paginationBody = new PaginationBody();
  addBody = new addBody();
  currentPage = 0;
  userList: UserList[] = [];
  serialNumber: number;
  totalItems: number;
  flags = {
    isAdded: false
  };
  getList = [];
  getallCategoryData = [];
  src3: any;
  day: string;
  timeFrom: string;
  timeTo: string;
  openingHoursData = [];
  dishImage: File;
  restaurantId: string;
  imageUrl: string;
  completedOrders: number;
  onGoingOrders: number;
  driverList = [];
  assignDriverOrderId: string;
  driverId: string;
  orderId:string;
  token:string;
  index:number;
  notificationCount:number;
  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private atp: AmazingTimePickerService,
    private url: UrlService,
    private router: Router,
    private observable: ObservableService
  ) { }

  ngOnInit() {
    this.imageUrl = this.url.imageUrl;
    this.totalItems = 0;
    this.serialNumber = 0;
    this.paginationBody.limit = 10;
    this.paginationBody.page = 0;
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.token = this.user.token
    this.localStorage.set('restaurantSection',4)
    this.getData();
  }
  getData() {
    this.app.show();
    this.paginationBody.limit = Number(this.paginationBody.limit);
    this.api.allOrders(this.paginationBody, this.user.token).subscribe((response: any) => {
      this.app.hide();
      if (!response.success) return this.error(response.message);
      this.getList = response.data;
      this.totalItems = response.total;
      this.completedOrders = response.completedOrders;
      this.onGoingOrders = response.onGoingOrders;
      this.getList.forEach(element => {
        element.total = Math.round(element.total * 100) / 100;
      });
   this.getNotificationsData();
    });
  }
  getNotificationsData() {
    this.app.show();
    this.api.getAllStatus(this.token).subscribe((response: any) => {
      this.app.hide();
      if (response.success == false) return this.error(response.message);
      this.notificationCount = response.notificationCount;
      this.localStorage.set('notificationData',response)
      this.observable.setCount(response)
    });
  }
 



  onChangeLimit() {
    this.app.show();
    this.getData();
  }

  pageChanged(e) {
    this.paginationBody.page = e.pageIndex;
    this.paginationBody.limit = e.pageSize;
    this.app.show();
    this.serialNumber = this.paginationBody.limit * this.paginationBody.page;
    this.getData();
  }

  view(val) {
    this.localStorage.set('orderDetail', val)

    this.router.navigateByUrl('/dashboard/ordersDetail')
  }

  serachList(val) {
    // if (val) {
    //   this.app.show();
    //   this.paginationBody.searchText = val;
    //   this.api.searchOrder(this.paginationBody, this.user.token).subscribe((response: any) => {
    //     this.app.hide();
    //     if (!response.success) return this.error(response.message);
    //     this.getList = response.data;
    //     this.totalItems = response.total;
    //     this.completedOrders = response.completedOrders;
    //     this.onGoingOrders = response.onGoingOrders;
    //     this.getList.forEach(element => {
    //       element.total = Math.round(element.total * 100) / 100;
    //     });
    //   });
    // }
    // else {
    //   this.getData();
    // }
  }
  assignDriverOpen(val,i) {
    this.orderId = val;
this.index = i;
    this.app.show();
    this.api.getAllDrivers(this.token).subscribe((response: any) => {
      this.app.hide();
      if (response.success == false) return this.error(response.message);
      this.driverList = response.data;
      $('#assigndDriverModal').modal('show');
    });
  }

  cancelOrderAdmin(userId, index) {
    swal({
      title: 'Are you sure?',
      text: 'You want to cancel this order ?',
      icon: 'warning',
      button: {
        text: 'Cancel',
        closeModal: false
      },
      dangerMode: true
    }).then((willDelete) => {
      if (willDelete) {
        this.cancelOrder(userId, index, false);
      }
    }, error => {
      this.app.hide();
      swal.close();
    });
  }
  cancelOrder(userId, index, type) { 
    this.app.show();
    this.api.updateOrderStatus({orderId:userId,orderStatus:8},this.token).subscribe((response: any) => {
      this.app.hide();
      if (!response.success) {
        this.error(response.message);
        return swal.close();
      }
       this.getList[index] = response.data;
      if (type) swal('SUCCESS', 'User blocked successfully!', 'success');
      if (!type) swal('SUCCESS', 'Order Cancelled successfully!', 'success');
     
    }, error => {
      this.app.hide();
    });
  }
  selectDrive(val) {
    this.driverId = val;
  }

  asignToDriver() {
    this.app.show();
    this.api.assignDriver({ driverId: this.driverId, orderId: this.orderId },this.token).subscribe((response: any) => {
      this.app.hide();
      if (response.success == false) return this.error(response.message);
      this.success(response.message)
      this.getList[this.index] = response.data;
      $('#assigndDriverModal').modal('hide');
    });
  }

  success = (message: string) => {
    this.toaster.successToastr(message);
  }
  error = (message: string) => {
    this.toaster.errorToastr(message);
  }

}


