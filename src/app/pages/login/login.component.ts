import { Component, OnInit } from '@angular/core';
import {LoginBody} from '../../requests/login-body';
import {AppComponent} from '../../app.component';
import {ApiService} from '../../services/api.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {LocalStorageService} from 'angular-web-storage';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginBody = new LoginBody();
  flags = {
    isLogin: false
  };

  constructor(
    private app: AppComponent,
    private api: ApiService,
    private toaster: ToastrManager,
    private localStorage: LocalStorageService,
    private router: Router
  ) { }

  ngOnInit() {
  }
  login() {
    this.app.show();
    this.flags.isLogin = true;
    this.loginBody.remember_me = false
    this.api.login(this.loginBody).subscribe((response: any) => {
      this.flags.isLogin = false;
      this.app.hide();
      if (!response.success) return this.error(response.message);
      this.localStorage.set('subscriptionBM_ADMIN', response.data);
      this.router.navigateByUrl('/dashboard/users');
    }, error => {
      this.app.hide();
      this.flags.isLogin = false;
    });
  }
  error = (message: string) => {
    this.toaster.errorToastr(message);
  }

}
