import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LocalStorageService } from 'angular-web-storage';
import { PaginationBody } from '../../requests/pagination-body';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UserList } from '../../model/userList';
import { AppComponent } from '../../app.component';
import { User } from '../../model/user';
import { addBody } from 'src/app/requests/vehicleprice/add';
import { updateBody } from 'src/app/requests/vehicleprice/update';
declare var $: any;
import * as _ from 'lodash';
import { UrlService } from 'src/app/services/url.service';
import * as js from '../../../assets/js/custom';
@Component({
  selector: 'app-vehicle-type',
  templateUrl: './vehicle-type.component.html',
  styleUrls: ['./vehicle-type.component.css']
})
export class VehicleTypeComponent implements OnInit {

  user = new User();
  paginationBody = new PaginationBody();
  addBody = new addBody();
  updateBody = new updateBody();
  currentPage = 0;
  userList: UserList[] = [];
  serialNumber: number;
  totalItems: number;
  flags = {
    isAdded: false
  };
  changeSmartServerStatus: boolean = false;
  carImagesFile: File;
  driverLicenceFile: File;
  vehicleRegistrationFile: File;
  proofOfInsuranceFile: File;
  smartserveImages: File;
  carImagesSrc: any;
  checkemail: boolean;
  driverLicenceSrc: any;
  vehicleRegistrationSrc: any;
  proofOfInsuranceSrc: any;
  smartserveImage: any;
  userId: string;
  message: string;
  imageUrl: string;
  file: File;
  src: any;
  token: string;
  showBoundaryLinks = true;
  totalCount: number;
  notificationCount: number;
  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private url: UrlService,
  ) { }

  ngOnInit() {
    this.imageUrl = this.url.imageUrl;
    this.totalItems = 0;
    this.serialNumber = 0;
    this.paginationBody.limit = 10;
    this.paginationBody.page = 0;
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.token = this.user.token
    this.app.show();
    this.getList();
    js.enterNumber();
    $('.SmartServeImg').hide();
  }
  getList() {
    this.paginationBody.limit = Number(this.paginationBody.limit);
    this.api.allVehiclePrice(this.paginationBody, this.user.token).subscribe((response: any) => {
      console.log(response)
      this.app.hide();
      if (!response.success) return this.error(response.message);
      this.totalItems = response.total;
      this.userList = response.result;
    }, error => {
      this.app.hide();
    });
  }


  add() {
    this.flags.isAdded = true;
    console.log(this.addBody)
    this.app.show()
    this.addBody.day = Number(this.addBody.day)
    this.api.addVehicleType(this.addBody, this.token).subscribe((response: any) => {
      console.log(response)
      this.flags.isAdded = false;

      $('#addModal').modal('hide')
      this.userList.push(response.data);
      this.totalItems = this.userList.length;
      this.app.hide()
      this.addBody = new addBody();
    })
  }

  edit(val, i) {
    console.log(val)
    this.updateBody.id = val._id;
    this.updateBody.vehicleName = val.vehicleName;
    this.updateBody.price = val.price;
    this.updateBody.userType = val.userType;
    this.updateBody.day = val.day;
    this.updateBody.withinKm = val.withinKm;
    this.updateBody.afterKmPrice = val.afterKmPrice;
    this.updateBody.index = i;
    $('#updateModal').modal('show')
  }
  update() {
    this.flags.isAdded = true;
    console.log(this.updateBody)
    this.updateBody.day = Number(this.updateBody.day)
    this.app.show()
    this.api.editVehicleType(this.updateBody, this.token).subscribe((response: any) => {
      console.log(response)
      this.flags.isAdded = false;

      $('#updateModal').modal('hide')
      this.userList[this.updateBody.index] = response.data;
      this.app.hide()
      this.updateBody = new updateBody();
    })
  }


  userType(val) {
    if (val == '0') {
      this.updateBody.userType = 0;
      this.addBody.userType = 0;
    } else if (val == '1') {
      this.updateBody.userType = 1;
      this.addBody.userType = 1;
    }
    else if (val == '2') {
      this.updateBody.userType = 2;
      this.addBody.userType = 2;
    }
  }

  pageChanged(e) {
    this.paginationBody.page = e.pageIndex;
    this.paginationBody.limit = e.pageSize;
    this.app.show();
    this.serialNumber = this.paginationBody.limit * this.paginationBody.page;
    this.getList();
  }



  error = (message: string) => {
    this.toaster.errorToastr(message);
  }
  success = (message: string) => {
    this.toaster.successToastr(message);
  }
}

