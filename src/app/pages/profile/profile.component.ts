import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import {LocalStorageService} from 'angular-web-storage';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AppComponent} from '../../app.component';
import {User} from '../../model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userData:string;
  user = new User();
  userList=[];

  flags = {
    isAdded: false
  };

  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private router:Router
  ) { }

  ngOnInit() {
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.app.show();
    this.getUserList();
  }
  getUserList() {
    this.userData = this.localStorage.get('profileData');
    
    this.userList = [this.userData];
    this.app.hide();
  }
  
  error = (message: string) => {
    this.toaster.errorToastr(message);
  }

}
