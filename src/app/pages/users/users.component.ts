import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LocalStorageService } from 'angular-web-storage';
import { PaginationBody } from '../../requests/pagination-body';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UserList } from '../../model/userList';
import { AppComponent } from '../../app.component';
import { User } from '../../model/user';
import { addBody } from 'src/app/requests/user/add';
import { updateBody } from 'src/app/requests/user/update';
import { Router } from '@angular/router';
declare var swal: any;
declare var $: any;
import * as _ from 'lodash';
import { UrlService } from 'src/app/services/url.service';
import { ObservableService } from 'src/app/services/observable.service';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  user = new User();
  paginationBody = new PaginationBody();
  addBody = new addBody();
  updateBody = new updateBody();
  currentPage = 0;
  userList: UserList[] = [];
  serialNumber: number;
  totalItems: number;
  flags = {
    isAdded: false
  };
  changeSmartServerStatus: boolean = false;
  carImagesFile: File;
  driverLicenceFile: File;
  vehicleRegistrationFile: File;
  proofOfInsuranceFile: File;
  smartserveImages: File;
  carImagesSrc: any;
  checkemail: boolean;
  driverLicenceSrc: any;
  vehicleRegistrationSrc: any;
  proofOfInsuranceSrc: any;
  smartserveImage: any;
  userId: string;
  message: string;
  imageUrl: string;
  file: File;
  src: any;
  token: string;
  showBoundaryLinks = true;
  totalCount: number;
  notificationCount: number;
  android_app_status: number
  ios_app_status: number
  id: string
  android_app_link:string=''
  ios_app_link:string=''
  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private router: Router,
    private url: UrlService,
    private observable: ObservableService
  ) { }

  ngOnInit() {
    this.imageUrl = this.url.imageUrl;
    this.totalItems = 0;
    this.serialNumber = 0;
    this.paginationBody.limit = 10;
    this.paginationBody.page = 0;
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.token = this.user.token
    this.app.show();
    this.getUserList();

    $('.SmartServeImg').hide();
  }
  getUserList() {
    this.paginationBody.limit = Number(this.paginationBody.limit);
    this.api.allUsers(this.paginationBody, this.user.token).subscribe((response: any) => {
      this.app.hide();
      if (!response.success) return this.error(response.message);
      this.totalItems = response.count;
      this.userList = response.data;
      this.userList.forEach(element => {
        if(element.subscription_details.stripe_customer_id == null){
          element.status = false
        }else{
          element.status = true
        }
        
      });
      // this.getNotificationsData();
    }, error => {
      this.app.hide();
    });
  }
  getNotificationsData() {
    this.app.show();
    this.api.getAllStatus(this.token).subscribe((response: any) => {
      this.app.hide();
      if (response.success == false) return this.error(response.message);
      this.notificationCount = response.notificationCount;
      this.localStorage.set('notificationData', response)
      this.observable.setCount(response)
    });
  }
  edit(val, i) {
    console.log(val)
    this.file = null;
    this.src = null
    this.updateBody.id = val._id;
    this.updateBody.name = val.name;
    this.updateBody.addressLine1 = val.addressLine1;
    this.updateBody.addressLine2 = val.addressLine2;
    this.updateBody.billingName = val.billingName;
    this.updateBody.billingAddress = val.billingAddress;
    this.updateBody.billingPhone = val.billingPhone;
    this.updateBody.buisness = val.buisness;
    this.updateBody.city = val.city;
    this.updateBody.email = val.email;
    this.updateBody.phone = val.phone;
    this.updateBody.points = val.points;
    this.updateBody.state = val.state;
    this.updateBody.userType = val.userType;
    // this.updateBody.profilePic = val.profilePic;
    this.updateBody.zipCode = val.zipCode;
    this.updateBody.index = i;
    $('#updateModal').modal('show')
  }
  update() {
    if(this.android_app_status == 4 && !this.android_app_link)  return this.warning('Please Enter Android App Link')
    if(this.ios_app_status == 4 && !this.ios_app_link) return this.warning('Please Enter IOS App Link')

    this.flags.isAdded = true;
    console.log(this.updateBody)
    this.app.show()
    const data = { app_id: this.id, android_app_status_number: this.android_app_status, ios_app_status_number: this.ios_app_status, android_app_link: this.android_app_link, ios_app_link: this.ios_app_link }
    this.api.update(data, this.token).subscribe((response: any) => {
      this.app.hide()
      if(response.success == false) return this.error(response.message)
      console.log(response)
      this.flags.isAdded = false;
      $('#updateModal').modal('hide')
      this.userList[this.updateBody.index] = response.user;
 
      this.updateBody = new updateBody();
    })
  }


  confirmEmail(driverEmail) {
    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    if (!pattern.test(driverEmail)) {
      return false;
    }
    else {
      return true;
    }
  }


  pageChanged(e) {
    this.paginationBody.page = e.pageIndex;
    this.paginationBody.limit = e.pageSize;
    this.app.show();
    this.serialNumber = this.paginationBody.limit * this.paginationBody.page;
    this.getUserList();
  }
  updateStatus(val, i) {
    this.updateBody.index = i
    console.log(val)
    this.id= val.app_info._id
    const item = val.app_info
    this.android_app_status = item.android_app_status
    this.ios_app_status = item.ios_app_status
    this.android_app_link = item.android_app_link
    this.ios_app_link = item.ios_app_link
    $('#updateModal').modal('show')
  }
  android_app_statusF(val) {
    this.android_app_status = Number(val)
    if(this.android_app_status != 4){
      this.android_app_link = ''
    }
    console.log(this.android_app_status)
  }
  ios_app_statusF(val) {
    this.ios_app_status = Number(val)
    if(this.ios_app_status != 4){
      this.ios_app_link = ''
    }
    console.log(this.ios_app_status)
  }

  goToDetail(val) {
    this.localStorage.set('userDetail', val);
    this.router.navigateByUrl('/dashboard/usersDetail');
  }

  error = (message: string) => {
    this.toaster.errorToastr(message);
  }
  warning = (message: string) => {
    this.toaster.warningToastr(message);
  }
  success = (message: string) => {
    this.toaster.successToastr(message);
  }
}
