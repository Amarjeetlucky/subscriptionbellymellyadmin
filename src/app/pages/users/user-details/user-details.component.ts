import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {LocalStorageService} from 'angular-web-storage';
import {PaginationBody} from '../../../requests/pagination-body';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AppComponent} from '../../../app.component';
import { AmazingTimePickerService } from 'amazing-time-picker';

declare var $:any;
import * as _ from 'lodash';
import { UrlService } from 'src/app/services/url.service';
import { Router } from '@angular/router';
import * as js from 'src/assets/js/custom';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  paginationBody = new PaginationBody();
  currentPage = 0;
  serialNumber: number;
  totalItems: number;
  flags = {
    isAdded: false
  };
  getList:any;

  src3:any;
  day:string;
  timeFrom:string;
  timeTo:string;
  openingHoursData=[];
  dishImage:File;
  restaurantId:string;
  imageUrl:string;
  user:string;
  completedOrderDetails:any;
  restaurantSection:number;
  orderData=[];
  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private atp: AmazingTimePickerService,
    private url:UrlService,
    private router:Router
  ) { }

  ngOnInit() {
    js.tabactive();
    this.imageUrl = this.url.imageUrl;
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.app.show();
    this.getData();
  }
  getData() {
      const data  = this.localStorage.get('userDetail');
      console.log(data)
      this.getList = data
      this.app.hide();
  }

  error = (message: string) => {
    this.toaster.errorToastr(message);
  }

}


