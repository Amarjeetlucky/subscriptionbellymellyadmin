import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LocalStorageService } from 'angular-web-storage';
import { PaginationBody } from '../../requests/pagination-body';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';
declare var $: any;
import * as _ from 'lodash';
import { UrlService } from 'src/app/services/url.service';
import { ObservableService } from 'src/app/services/observable.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  token: string;
  notificationCount: number;
  notificationList: any;
  user: any
  constructor(
    private api: ApiService,
    private app: AppComponent,
    private localStorage: LocalStorageService,
    private toaster: ToastrManager,
    private router: Router,
    private url: UrlService,
    private observable: ObservableService
  ) { }

  ngOnInit() {
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.token = this.user.token
    this.app.show();
    this.getNotificationsData();
  }

  getNotificationsData() {
    this.app.show();
    this.api.getAllStatus(this.token).subscribe((response: any) => {
      this.app.hide();
      if (response.success == false) return this.error(response.message);
      this.notificationCount = response.notificationCount;
      this.notificationList = response.notifications;
      this.localStorage.set('notificationData', response)
      this.observable.setCount(response)
      this.markNotificationReadF();
    });
  }


  markNotificationReadF() {
    this.api.markNotificationRead(this.token).subscribe((response: any) => {
    })
  }
  deleteNotificationsF() {
    this.app.show();
    this.api.deleteNotifications(this.token).subscribe((response: any) => {
      this.app.hide();
      if (response.success == false) return this.error(response.message);
      this.success(response.message)
    })
  }



  error = (message: string) => {
    this.toaster.errorToastr(message);
  }
  success = (message: string) => {
    this.toaster.successToastr(message);
  }
}
