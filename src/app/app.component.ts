import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { SocketService } from "./services/socket.service";
declare var $: any;
declare var Audio: any;
import { Howl, Howler } from 'howler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  @ViewChild('mp3', null) mp3: ElementRef;

  title = 'BellyMelly Subscription';

  constructor(
    private spinner: NgxSpinnerService,
    private socket: SocketService
  ) {
    // socket.onNewOrder().subscribe((response: any) => {
    //   this.playSound();
    // });
  }
  ngAfterViewInit(): void {
  }

  show() {
    this.spinner.show();
  }
  hide() {
    this.spinner.hide();
  }
  playSound() {
    console.log("playSound");
    this.mp3.nativeElement.loop = true;
    const playPromise = this.mp3.nativeElement.play();
    if (playPromise !== undefined) {
      playPromise.then(function () {
        $('#confirmationModal').modal({
          backdrop: 'static',
          keyboard: false
        });
      }).catch(function (error) {

      });
    }

  }
  pauseSound() {
    this.mp3.nativeElement.pause()
    this.mp3.nativeElement.currentTime = 0;
    $('#confirmationModal').modal('hide');
  }
}
