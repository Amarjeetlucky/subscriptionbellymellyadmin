import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { UsersComponent } from './pages/users/users.component';
import {ToastrModule} from 'ng6-toastr-notifications';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClient} from '@angular/common/http';
import {InterceptorService} from './services/interceptor.service';
import { LoginComponent } from './pages/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BsDatepickerModule, PaginationModule} from 'ngx-bootstrap';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import { ProfileComponent } from './pages/profile/profile.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
// import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {MatPaginatorModule} from '@angular/material';
import {AmazingTimePickerModule} from 'amazing-time-picker';
import { TranslateModule,TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrderDetailComponent } from './pages/orders/order-detail/order-detail.component';
import { NgxEditorModule } from 'ngx-editor';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { VehicleTypeComponent } from './pages/vehicle-type/vehicle-type.component';
import { StripeModule } from "stripe-angular";
import { UserDetailsComponent } from './pages/users/user-details/user-details.component';
import { BlogComponent } from './pages/blog/blog.component';
export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideBarComponent,
    BodyComponent,
    FooterComponent,
    UsersComponent,
    LoginComponent,
    OnlyNumberDirective,
    ProfileComponent,
    OrdersComponent,
    OrderDetailComponent,
    NotificationsComponent,
    VehicleTypeComponent,
    UserDetailsComponent,
    BlogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    PaginationModule.forRoot(),
    NgxSpinnerModule,
    GooglePlaceModule,
    NgxEditorModule,
    // NgMultiSelectDropDownModule.forRoot(),
    StripeModule.forRoot("pk_test_lxafn9FEXKMyuTNAJZ7P69Vu00Yz4HDQOd"),
    MatPaginatorModule,
    BsDatepickerModule.forRoot(),
    AmazingTimePickerModule,
    TranslateModule.forRoot({
      loader:{
        provide:TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps:[HttpClient]
      }
    })
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
