export class placeOrder {
  name: string;
  email: string;
  phone: string;
  billingAddress: string;
  billingName: string;
  billingPhone: string;
  addressLine1: string;
  addressLine2: string;
  state: string;
  city: string;
  zipCode: string;
  pickupLocation: string;
  pickupLocationLat: number;
  pickupLocationLong: number;
  destinationAddress: string;
  destinationLat: number;
  destinationLong: number;
  senderName: string;
  senderPhone: string;
  recieverName: string;
  recieverPhone: string;
  carType: number=0;
  vehicleName:string;
  deliveryDay: number=0;
  timeSlot: string;
  deliveryInstruction: string;
  totalAmount: number;
  receiverName: string;
  senderBuilding: string;
  recieverBuilding: string;
  pickedTime: string;
  token: string;
}

