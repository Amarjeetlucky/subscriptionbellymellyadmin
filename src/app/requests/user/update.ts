export class updateBody{
    id:string;
    userType:number;
    name:string;
    addressLine1:string;
    addressLine2:string;
    billingAddress:string;
    billingName:string;
    billingPhone:string;
    buisness:string;
    city:string;
    email:string;
    phone:string;
    points:string;
    state:string;
    zipCode:string;
    profilePic:string;
    index:number;
}

