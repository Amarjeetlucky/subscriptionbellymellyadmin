export class addBody{
    name:string;
    classificationCategory:string;
    description:string;
    availableFrom:string;
    availableTo:string; 
    restaurantId:string;
    price:number;
    isAvailable:boolean;
}