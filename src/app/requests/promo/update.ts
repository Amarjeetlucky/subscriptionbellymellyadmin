export class updateBody{
    id:string;
    promocode:string;
    startDate:string;
    endDate:string;
    usageType:number;
    usageTime:number; 
    discountPercentage:number;
    maxDiscount:number;
    minSpendNeed:number;
    index:number;
}

