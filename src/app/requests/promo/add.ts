export class addBody{
    promocode:string;
    startDate:string;
    endDate:string;
    usageType:number = 0;
    usageTime:number; 
    discountPercentage:number;
    maxDiscount:number;
    minSpendNeed:number;
}