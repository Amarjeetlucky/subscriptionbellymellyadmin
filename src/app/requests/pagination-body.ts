export class PaginationBody {
  limit: number;
  page: number;
  skip: number;
  searchText: string;
  restaurantId:string;
  storeId:string;
  liquorStoreId:string;
  driverId:string;
  startDateLong:number;
  endDateLong:number;
  isApproved:boolean;
  id:string;
}
