import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodyComponent } from './components/body/body.component';
import { UsersComponent } from './pages/users/users.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrderDetailComponent } from './pages/orders/order-detail/order-detail.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { VehicleTypeComponent } from './pages/vehicle-type/vehicle-type.component';
import { UserDetailsComponent } from './pages/users/user-details/user-details.component';
import { BlogComponent } from './pages/blog/blog.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'dashboard', component: BodyComponent, children: [
      { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
      { path: 'usersDetail', component: UserDetailsComponent, canActivate: [AuthGuard] },
      { path: 'blog', component: BlogComponent, canActivate: [AuthGuard] },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard] },
      { path: 'ordersDetail', component: OrderDetailComponent, canActivate: [AuthGuard] },
      { path: 'notification', component: NotificationsComponent, canActivate: [AuthGuard] },
      { path: 'vehicle', component: VehicleTypeComponent, canActivate: [AuthGuard] },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
