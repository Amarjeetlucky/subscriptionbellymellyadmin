import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UrlService} from './url.service';
import {LoginBody} from '../requests/login-body';
import {PaginationBody} from '../requests/pagination-body';




@Injectable({
  providedIn: 'root'
})
export class ApiService {
  post(url: string, body: any, token?) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token || ''
      })
    };
    return this.http.post(url, body, headers);
  }
  patch(url: string, body: any, token:any) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token || ''
      })
    };
    return this.http.patch(url, body, headers);
  }
  get(url: string, token?) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token || ''
      })
    };
    return this.http.get(url, headers);
  }
  constructor(private url: UrlService, private http: HttpClient) { }

  allOrders(body: any,token:any) {
    return this.post(this.url.allOrders, body,token);
  }
  editUser(body: any,token:any) {
    return this.post(this.url.editUser, body,token);
  }
  allDrivers(body: any,token:any) {
    return this.post(this.url.allDrivers, body,token);
  }
  approveDrivers(body: any,token:any) {
    return this.post(this.url.approveDrivers, body,token);
  }
  assignDriver(body: any,token:any) {
    return this.post(this.url.assignDriver, body,token);
  }
  updateOrderStatus(body: any,token:any) {
    return this.post(this.url.updateOrderStatus, body,token);
  }
  getAllDrivers(token:any) {
    return this.get(this.url.getAllDrivers,token);
  }
  getAllStatus(token:any) {
    return this.get(this.url.getAllStatus,token);
  }
  allVehiclePrice(body: any,token:any) {
    return this.post(this.url.allVehiclePrice, body,token);
  }
 
  addVehicleType(body: any,token:any) {
    return this.post(this.url.addVehicleType, body,token);
  }
  editVehicleType(body: any,token:any) {
    return this.post(this.url.editVehicleType, body,token);
  }
  addDeliveryPrice(body: any,token:any) {
    return this.post(this.url.addDeliveryPrice, body,token);
  }
  editDeliveryPrice(body: any,token:any) {
    return this.post(this.url.editDeliveryPrice, body,token);
  }
  allDeliveryPrice(body: any,token:any) {
    return this.post(this.url.allDeliveryPrice, body,token);
  }

  markNotificationRead(token:any) {
    return this.get(this.url.markNotificationRead, token);
  }
  deleteNotifications(token:any) {
    return this.get(this.url.deleteNotifications,token);
  }
  

  getPromocodes(body: any,token:any) {
    return this.post(this.url.getPromocodes, body,token);
  }

  addPromocode(body: any,token:any) {
    return this.post(this.url.addPromocode, body,token);
  }

  editPromocode(body: any,token:any) {
    return this.post(this.url.editPromocode, body,token);
  }

  activeDeactivePromo(body: any,token:any) {
    return this.post(this.url.activeDeactivePromo, body,token);
  }
  allPrices(token:any) {
    return this.get(this.url.allPrices, token);
  }
  sendInvoice(body: any,token:any) {
    return this.post(this.url.sendInvoice, body,token);
  }
  createOrder(body: any,token:any) {
    return this.post(this.url.createOrder, body,token);
  }
  emailExists(body: any,token:any) {
    return this.post(this.url.emailExists, body,token);
  }
  
  //// New
  login(body: any) {
    return this.http.post(this.url.login, body);
  }
  allUsers(body: any,token:any) {
    return this.post(this.url.allUsers, body,token);
  }
  update(body: any,token:any) {
    return this.patch(this.url.update, body,token);
  }
  writeblog(body: any,token:any) {
    return this.post(this.url.writeblog, body,token);
  }
  getBlog(token:any) {
    return this.get(this.url.getBlog, token);
  }
  getBlogs(body: any,token) {
    return this.post(this.url.getBlogs,body,token);
  }
}
