import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  // baseUrl = 'https://bellymelly.jugaldb.com/admin'; //live
  // baseUrl= 'https://bellymelly-testing.herokuapp.com/admin' //testing
  baseUrl= 'https://api-belly-melly.herokuapp.com/admin' //testing

  imageUrl = '';
  
  constructor() { }
 
  allOrders = `${this.baseUrl}/allOrders`;
  editUser = `${this.baseUrl}/editUser`;
  allDrivers = `${this.baseUrl}/allDrivers`;
  approveDrivers = `${this.baseUrl}/approveDrivers`;
  assignDriver = `${this.baseUrl}/assignDriver`;
  updateOrderStatus = `${this.baseUrl}/updateOrderStatus`;
  getAllDrivers = `${this.baseUrl}/getAllDrivers`;
  getAllStatus = `${this.baseUrl}/getAllStatus`;
  allVehiclePrice = `${this.baseUrl}/allVehiclePrice`;
  addVehicleType = `${this.baseUrl}/addVehicleType`;
  editVehicleType = `${this.baseUrl}/editVehicleType`;
  allDeliveryPrice = `${this.baseUrl}/allDeliveryPrice`;
  addDeliveryPrice = `${this.baseUrl}/addDeliveryPrice`;
  editDeliveryPrice = `${this.baseUrl}/editDeliveryPrice`;
  markNotificationRead = `${this.baseUrl}/markNotificationRead`;
  deleteNotifications = `${this.baseUrl}/deleteNotifications`;

  getPromocodes = `${this.baseUrl}/getPromocodes`;
  addPromocode = `${this.baseUrl}/addPromocode`;
  editPromocode = `${this.baseUrl}/editPromocode`;
  activeDeactivePromo = `${this.baseUrl}/activeDeactivePromo`;
  allPrices = `${this.baseUrl}/allPrices`;
  sendInvoice = `${this.baseUrl}/sendInvoice`;
  createOrder = `${this.baseUrl}/createOrder`;
  emailExists = `${this.baseUrl}/emailExists`;

  ////New

  login = `${this.baseUrl}/login`;
  allUsers = `${this.baseUrl}/userDetails`;
  update = `${this.baseUrl}/app/update`;
  writeblog = `${this.baseUrl}/blog/writeblog`;
  getBlog = `${this.baseUrl}/blog/getBlog`;
  getBlogs = `${this.baseUrl}/blog/getBlogs`; 
}
