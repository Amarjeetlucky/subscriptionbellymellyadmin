import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {LocalStorageService} from "angular-web-storage";
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ObservableService {
  locationDetails:  BehaviorSubject<any>;
  constructor(private localStorage: LocalStorageService) {
    if (this.localStorage.get('notificationData')) {
      this.locationDetails = new BehaviorSubject<any>(this.localStorage.get('notificationData'));
    } else {
      this.locationDetails = new BehaviorSubject<any>(null);
    }
  }

  getCount(): Observable<any> {
    return this.locationDetails.asObservable();
  }
  setCount(data: any) {
    this.locationDetails.next(data);
  }
}
