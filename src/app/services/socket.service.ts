import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: any;

  constructor() {
    // this.socket = io.connect('https://assuredparcel.com');
  }
  onNewOrder() {
    return new Observable(observer => {
      this.socket.on('newOrder', order => {
        observer.next(order);
      });
    });
  }

  getCalculation(val) {
    var a = val * Math.pow(10, 2);
    var b = Math.round(a);
    var c = b / Math.pow(10, 2)
    return (c)
  }
}
