import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {LocalStorageService} from 'angular-web-storage';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  constructor(
    private router: Router,
    private toaster: ToastrManager,
    private localStorage: LocalStorageService
  ) { }
  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (error: any) => {
      if (error instanceof HttpErrorResponse) {
        // console.log(error)
        if (error.status === 403) {
          this.toaster.errorToastr('Your session is expired, please sign in.');
          this.localStorage.set('subscriptionBM_ADMIN', null)
          this.router.navigateByUrl('/');
        } else if (error.status !== 200) {
          return alert(error.message);
        }
      }
    }));
  }
}
