export class User {
  createdAt: string;
  email: string;
  mobileNumber: string;
  name: string;
  token: string;
  updatedAt: string;
  _id: string;
}
