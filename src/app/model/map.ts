import {UserList} from './userList';

export class Map {
  _id: string;
  name: string;
  status: number;
  participants: any;
  winner: UserList;
  latitude: number;
  huntStartDate: number;
  bonusPack: number;
  longitude: number;
  isBonusClue: boolean;
  createdAt: string;
  updatedAt: string;
  id: string;
}
