export class CouponList {
  usedBy: any;
  discountType: number;
  status: number;
  _id: string;
  category: string;
  name: string;
  discount: number;
  notes: string;
  startDate: number;
  endDate: number;
  couponType: number;
  createdAt: string;
  updatedAt: string;
  id: string;
}
