import {UserList} from './userList';

export class RiddleList {
  isClaimedFirst: number;
  isCompleted: number;
  completedBy: any;
  _id: string;
  seriesNumber: number;
  mapId: string;
  huntId: string;
  clue: string;
  threeWords: any;
  tier: number;
  riddle: string;
  riddleBy: any;
  question: string;
  answer: string;
  answerStatus: boolean;
  createdAt: string;
  updatedAt: string;
  isPrize: boolean;
  isLocation: boolean;
  isClaimedFirstBy: UserList;
  id: string;

}
