import {RiddleList} from './riddle-list';

export class LineList {
  _id: string;
  id: string;
  line: string;
  riddle: RiddleList;
  createdAt: string;
  updatedAt: string;
  isBonusLine: boolean;
}
