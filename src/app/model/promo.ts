export class Promo {
    createdAt: string;
    promoCode: string;
    discountPercentage: number;
    promoType: number;
    isActive: boolean;
    updatedAt: string;
    _id: string;
  }
  