export class UserList {
  meta_value: [];
  _id: string;
  ID: string;
  user_email: string;
  display_name: string;
  isBan: boolean;
  user_login: string;
  firstName: string;
  lastName: string;
  phone: string;
  dob: number;
  user_pass: string;
  user_nicename: string;
  user_url: string;
  user_activation_key: string;
  user_status: string;
  meta_key: string;
  createdAt: string;
  updatedAt: string;
  address:string;
  subscription_details:any
  status:boolean
}
