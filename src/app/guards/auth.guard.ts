import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import {LocalStorageService} from 'angular-web-storage';
import {ToastrManager} from 'ng6-toastr-notifications';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private localStorage: LocalStorageService,
    private router: Router,
    private toaster: ToastrManager
  ) {
  }
  canActivate() {
    if (this.localStorage.get('subscriptionBM_ADMIN')) {
      return true;
    }
    this.toaster.errorToastr('Sorry, you need to sign in first.');
    this.router.navigate(['/']);
    return false;
  }
}
