import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  @Output() routeChange = new EventEmitter();
  menuList = [
    { path: '/dashboard/home', title: 'Dashboard',  icon: 'ti-panel', class: '' },
    { path: '/dashboard/users', title: 'Users',  icon: 'ti-user', class: '' },
    { path: '/dashboard/map', title: 'Map',  icon: 'ti-map', class: '' },
    { path: '/dashboard/coupon', title: 'Coupon',  icon: 'ti-list', class: '' },
  ];
  constructor() { }

  ngOnInit() {
  }
  emit(url) {
    this.routeChange.emit(window.location.hash);
  }

}
