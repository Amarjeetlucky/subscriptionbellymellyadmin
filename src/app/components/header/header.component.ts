import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { UrlService } from 'src/app/services/url.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import { User } from '../../model/user';
import { Observable } from 'rxjs';
import { ObservableService } from 'src/app/services/observable.service';
import { SocketService } from "../../services/socket.service";

declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user = new User();
  token: string;
  getList: any;
  notificationCount: number
  notificationData: any;
  constructor(
    private router: Router,
    private localStorage: LocalStorageService,
    private api: ApiService,
    private app: AppComponent,
    private toaster: ToastrManager,
    private url: UrlService,
    private observable: ObservableService,
    private socket: SocketService
  ) {
  }

  ngOnInit() {
    this.user = this.localStorage.get('subscriptionBM_ADMIN');
    if (!this.user) return;
    this.token = this.user.token
    this.getNotificationCount();
    // this.socket.onNewOrder().subscribe((response: any) => {
    //   this.getNotificationCount();
    // });
  }
  logout() {
    this.localStorage.set('subscriptionBM_ADMIN', null)
    this.router.navigateByUrl('/');
  }
  openNav() {
    $('.sidebar').css('display', 'block')
    $('.sidebar').css('width', '260px')
  }
  success = (message: string) => {
    this.toaster.successToastr(message);
  }
  error = (message: string) => {
    this.toaster.errorToastr(message);
  }
  getNotificationCount() {
    this.observable.getCount().subscribe((response: any) => {
      if (response) {
        console.log(response)
        this.notificationCount = response.notificationCount;
        this.notificationData = response.notifications;
      }
    });
  }
}
