import { Component, OnInit } from '@angular/core';
import * as js from 'src/assets/js/custom';
import {LocalStorageService} from 'angular-web-storage';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  selectedPath: string;
  menuList = [
    { path: '/dashboard/home', title: 'Dashboard',  icon: 'ti-panel', class: '' },
    { path: '/dashboard/users', title: 'Users',  icon: 'ti-user', class: '' },
    { path: '/dashboard/map', title: 'Map',  icon: 'ti-map', class: '' },
    { path: '/dashboard/coupon', title: 'Coupon',  icon: 'ti-list', class: '' },
  ];

  constructor(
    private localStorage: LocalStorageService,
    private router: Router,
  ) { }

  ngOnInit() {
   
    if(innerWidth <= 991){
      js.closesidebar();
    }
    
   
    const parts = window.location.hash.replace('#/', '/').split('/');
    if (parts.length < 2) {
      this.selectedPath = parts[1];
    } else {
      this.selectedPath = `/${parts[1]}/${parts[2]}`;
    }
  }
  closeNav(){
    // $('.sidebar').css('display','none')
    $('.sidebar').css('width','0px')
}
logout() {
  this.localStorage.set('subscriptionBM_ADMIN', null)
  this.router.navigateByUrl('/');
}
  
}
